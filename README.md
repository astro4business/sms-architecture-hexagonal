# **Architecture logicielle**

![Architecture Hexagonal](./images/391d9c3ed373c597ca4873279036448d82d2ce627e9d53e431facf41de47de98.png) 
L’architecture d’un logiciel décrit la manière dont seront agencés les différents éléments d’une application et comment ils interagissent entre eux. Cette étape est donc l’une des premières étapes du développement logiciel et intervient lors de la phase de conception.

### **Caracteristique d'une bonne architecture logiciel**

Une bonne architecture, se définit par :

#### **Testabilité**

> La première des qualité d'une bonne application est de permettre de tester l'application de façon automatique (tester dans un environnement qui n'est pas celui de l'environnement de production avec des données proches de celle de la production).

#### **Évolution**

> L’architecture doit prendre en compte les évolutions futures du logiciel en fonction du besoin métier. Si on ne peut anticiper les évolutions elles-mêmes, elle doit dans ce cas être assez souple pour permettre une possible évolution.
>
> Une bonne architecture est indépendante des choix techniques si un choix technique est impossible voir trop couteux c'est dire que l'architecture n'est pas evolutive.

#### **Simplicité**

> Une architecture complexe est souvent source de défaillance et peut créer de la dette technique, impacter les performances ou l’évolutivité d’une application. Elle est due à une mauvaise conception, une sur-ingénierie initiale ou à l’inverse un manque de conception global qui induit une complexification progressive du logiciel dans le temps.
>
> De plus, le logiciel doit avoir une architecture « compréhensible » pour faciliter sa prise en main. Pour cela, il est nécessaire de :
***respecter les standards afin qu’il soit possible pour une personne ne connaissant pas le projet d’intervenir.***

#### **Maintenabilité**

> Une bonne architecture intègre aussi l’outillage nécessaire à sa maintenance. Cela permet notamment de récupérer de l’information de manière centralisée lorsqu’il y a une erreur afin de pouvoir la traiter efficacement et d’agir en conséquence.

# **Architecture Hexagonal**

L'architecture hexagonale, ou architecture à base de ports et d'adaptateurs, est un patron d'architecture utilisé dans le domaine de la conception des logiciels. Elle vise à créer des systèmes à base de composants d'application qui sont **faiblement couplés** et qui peuvent être facilement connectés à leur environnement logiciel au moyen de ports et d'adaptateurs. Ces composants sont modulaires et interchangeables ce qui renforce la cohérence des traitements et facilite l'automatisation des tests.

*Source* : [wikipedia](https://fr.wikipedia.org/wiki/Architecture_hexagonale_(logiciel)#:~:text=L'architecture%20hexagonale%20a%20%C3%A9t%C3%A9,de%20l'interface%20utilisateur%20avec)

## **Principes de l’Architecture Hexagonale**

L’architecture hexagonale s’appuie sur trois principes et techniques :

- Séparer explicitement User-Side, Business Logic et Server-Side
- Les dépendances vont vers la Business Logic
- On isole les frontières par des Ports et Adapters

![Architecture Hexagonal](./images/bd7d877db2f55b36c43904987cdd8a1880fbbd69032fe2f6bbf7dd617a8eb321.png)

**User-Side**

C’est le côté par lequel l’utilisateur ou les programmes extérieurs vont interagir avec l’application. On y trouve le code qui permet ces interactions. Typiquement, votre code d’interface utilisateur, vos routes HTTP pour une API, vos sérialisations en JSON à destination de programmes qui consomment votre application sont ici.

**Business Logic**

C’est la partie que l’on veut isoler de ce qui est à gauche et à droite. On y trouve tout le code qui concerne et implémente la logique métier. Le vocabulaire métier et la logique purement métier, ce qui se rapporte au problème concret que résout votre application, tout ce qui en fait la richesse et la spécificité est au centre. Dans l’idéal, un expert du métier qui ne sait pas coder pourrait lire un bout de code de cette partie et vous pointer une incohérence (true story, ce sont des choses qui pourraient vous arriver !).

**Server-Side**

C’est ici qu’on va retrouver ce dont votre application a besoin, ce qu’elle pilote pour fonctionner. On y trouve les détails d’infrastructure essentiels comme le code qui interagit avec votre base de données, les appels au système de fichier, ou le code qui gère des appels HTTP à d’autres applications dont vous dépendez par exemple.

![Architecture Hexagonal](./images/7c5b765e2edd48901d664f690425cf4af7e0a1ce212d9f1bc2c3da961438ebf9.png)

## Principe Architectural

En plus de rendre le métier indépendant des systèmes extérieurs, les interface permettent de satisfaire le fameux D de SOLID, ou Dependency Inversion Principle. Ce principe dit :

> Les modules de haut niveau ne doivent pas dépendre des modules de bas niveau. Les deux doivent dépendre d’abstractions.

> Les abstractions ne doivent pas dépendre des détails. Les détails doivent dépendre des abstractions.

Si on n’avait pas l’interface, on aurait un module de haut niveau (la Business Logic) qui dépendrait d’un module de bas niveau (le Server-Side).

### **Qu'est ce qu'un port ?**

Un port est un point d'entrée et de sortie indépendant du consommateur vers/depuis l'application. Dans de nombreuses language, ce sera representé par une interface. Par exemple, il peut s'agir d'une interface utilisée pour effectuer des recherches dans un moteur de recherche. Dans notre application, nous utiliserons cette interface comme point d'entrée et/ou de sortie sans aucune connaissance de l'implémentation concrète qui sera réellement injectée là où l'interface est définie.

### **Qu'est ce qu'un adapteur ?**

Un adaptateur est une classe qui transforme (adapte) une interface en une autre.

### **Inversion de contrôle**

Une caractéristique à noter sur ce pattern est que les adaptateurs dépendent d'un outil spécifique et d'un port spécifique (en implémentant une interface). Mais notre logique métier ne dépend que du port (interface), qui est conçu pour répondre aux besoins de la logique métier, elle ne dépend donc pas d'un adaptateur ou d'un outil spécifique.

Cela signifie que la direction des dépendances est vers le centre donc vers la logique métier.

### Découplage des composants

Pour découpler les classes, nous utilisons l'injection de dépendances, en injectant des dépendances dans une classe au lieu de les instancier à l'intérieur de la classe, et l'inversion de dépendances, en faisant dépendre la classe d'abstractions (interfaces et/ou classes abstraites) au lieu de classes concrètes. Cela signifie que la classe dépendante n'a aucune connaissance de la classe concrète qu'elle va utiliser, elle n'a aucune référence au nom de classe complet des classes dont elle dépend.

#### **Comment on organise le code à l’intérieur et à l’extérieur ?**

**Structure**
Identifier les domaines métier, les domaines partagés entre divers domaines métier afin de facilité l'organisation des modules (ou répertoires) en fonction de la logique métier.

![Architecture Hexagonal](./images/f228e9edba5913ac500bb0f127c5983762753b7be3fd502f20d3b0423511c9fc.png)
