package api.sms.astro.contact.application.casutilisation;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import api.sms.astro.contact.application.CreerContact;
import api.sms.astro.contact.application.port.in.ContactCommande;
import api.sms.astro.contact.domaine.Contact;
import api.sms.astro.contact.domaine.Pays;
import api.sms.astro.contact.domaine.Ville;
import api.sms.astro.contact.application.port.out.ContactPortRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 25/08/2020
 **/
@ExtendWith(MockitoExtension.class)
class CreerContactTest {

  @Mock
  ContactPortRepository contactPortRepository;

  // Given
  Pays pays;
  Ville ville;

  CreerContact creerContact;
  ContactCommande commande;

  @BeforeEach
  void setUp() {

    // Given
    this.pays = new Pays();
    this.ville = new Ville();

    this.creerContact = new CreerContact(this.contactPortRepository);

    this.commande = new ContactCommande();
  }

  @Test
  @DisplayName("Test pour la génération d'une classe contact")
  void genererContact() {

    // Given
    this.pays.ajouterVille(ville);

    // When
    Contact contact = this.creerContact.genererContact(this.commande);

    // Then
    assertNotNull(contact);
  }

  @Test
  @DisplayName("Test pour la génération d'une classe contact en cas d'erreur")
  void genererContactErreurVille() {

    // Todo : tester avec le cas d'erreur

    // When
    Contact contact = this.creerContact.genererContact(this.commande);

    // Then
    assertNull(contact);
  }

  @Test
  @DisplayName("Test pour la création d'un contact")
  void creer() {

    // Given
    this.pays.ajouterVille(this.ville);

    Mockito.doNothing().when(this.contactPortRepository)
        .enregistrer(Mockito.any(Contact.class));

    // When
    this.creerContact.creer(this.commande);

    // Then
    Mockito.verify(this.contactPortRepository, Mockito.times(1))
        .enregistrer(Mockito.isA(Contact.class));
  }
}
