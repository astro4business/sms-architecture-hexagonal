package api.sms.astro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class AstroApplication {

  public static void main(String[] args) {
    SpringApplication.run(AstroApplication.class, args);
  }

}
