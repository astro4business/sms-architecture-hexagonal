package api.sms.astro;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 15/08/2020
 **/
public interface GestionnaireCommande<T> {
  void execute(T commande);
}
