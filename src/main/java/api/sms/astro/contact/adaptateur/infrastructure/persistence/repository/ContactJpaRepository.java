package api.sms.astro.contact.adaptateur.infrastructure.persistence.repository;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.ContactEntity;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@Repository
public interface ContactJpaRepository extends JpaRepository<ContactEntity, UUID> {
  ContactEntity findContactEntityById(UUID id);
  ContactEntity findContactEntityByEmail(String email);
  List<ContactEntity> findContactEntityBySupprimerFalse();
}
