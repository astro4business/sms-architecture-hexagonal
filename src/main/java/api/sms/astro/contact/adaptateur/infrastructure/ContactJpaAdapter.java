package api.sms.astro.contact.adaptateur.infrastructure;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.ContactEntity;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.mapper.ContactMapper;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.repository.ContactJpaRepository;
import api.sms.astro.contact.application.port.out.ContactPortRepository;
import api.sms.astro.contact.domaine.Contact;
import java.util.List;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@Repository
public class ContactJpaAdapter implements ContactPortRepository {

  private final ContactJpaRepository contactJpaRepository;
  ContactMapper mapper = Mappers.getMapper(ContactMapper.class);

  public ContactJpaAdapter(
      ContactJpaRepository contactJpaRepository) {
    this.contactJpaRepository = contactJpaRepository;
  }

  @Override
  public void enregistrer(Contact contact) {

    if (contact.getId() != null){
      ContactEntity contactEntity = this.contactJpaRepository.findContactEntityById(contact.getId());
      if (contactEntity != null) {
        ContactEntity contactEntityClient = mapper.contactToEntity(contact);
        this.contactJpaRepository.save(contactEntityClient);
      }
      else {}
        // Todo : Exception en cas de non existance d'un contact;
    } else {
      ContactEntity contactEntity = mapper.contactToEntity(contact);
      contactEntity = this.contactJpaRepository.save(contactEntity);
      System.out.print(contactEntity);
    }
  }

  @Override
  public void supprimer(Contact contact) {
    contact.setSupprimer(true);
    this.enregistrer(contact);
  }

  @Override
  public List<Contact> afficherListesContacts() {

    List<ContactEntity> contactEntity = this.contactJpaRepository
        .findContactEntityBySupprimerFalse();
    return mapper.entitiesToContacts(contactEntity);
  }
}
