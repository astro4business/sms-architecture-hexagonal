package api.sms.astro.contact.adaptateur.infrastructure.persistence.entite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> Objet valeur permettant l'enregistrement d'un pays </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Entity
@Table(name = "pays")
@Getter
@Setter
@NoArgsConstructor
public class PaysEntity implements Serializable {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column(unique = true, nullable = false)
  private String nom;

  @Column(unique = true)
  private String coordonnee;

  @Column(unique = true)
  private String codeISO;

  @OneToMany(cascade = CascadeType.ALL,
      fetch = FetchType.LAZY)
  private List<VilleEntity> villes = new ArrayList<>();

  @Column(columnDefinition = "BOOLEAN DEFAULT false")
  private boolean supprimer;
}
