package api.sms.astro.contact.adaptateur.infrastructure.persistence.mapper;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.AdresseEntity;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.ContactEntity;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.PaysEntity;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.VilleEntity;
import api.sms.astro.contact.domaine.Adresse;
import api.sms.astro.contact.domaine.Contact;
import api.sms.astro.contact.domaine.Pays;
import api.sms.astro.contact.domaine.Ville;
import java.util.List;
import org.mapstruct.Mapper;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@Mapper(componentModel = "spring")
public interface ContactMapper {

  Contact entityToContact(ContactEntity contactEntity);

  ContactEntity contactToEntity(Contact contact);

  Adresse entityToAdresse(AdresseEntity adresseEntity);

  AdresseEntity adresseToEntity(Adresse adresse);

  Pays entityToPays(PaysEntity paysEntity);

  PaysEntity paysToEntity(Pays pays);

  Ville entityToVille(VilleEntity villeEntity);

  VilleEntity villeToEntity(Ville ville);

  List<Contact> entitiesToContacts(List<ContactEntity> contactEntities);

  List<ContactEntity> contactsToEntities(List<Contact> contacts);
}
