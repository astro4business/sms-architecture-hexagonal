package api.sms.astro.contact.adaptateur.infrastructure.persistence.entite;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <p> Entité domaine Contact </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Entity
@Table(name = "contact")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ContactEntity implements Serializable {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column
  private String email;

  @Column
  private String numeroTelephone;

  @Column
  private String numeroFixe;

  @Embedded
  private AdresseEntity adresse;

//  @OneToMany
//  private UtilisateurEntity utilisateur;

  @Column(columnDefinition = "BOOLEAN DEFAULT false")
  private boolean supprimer;
}
