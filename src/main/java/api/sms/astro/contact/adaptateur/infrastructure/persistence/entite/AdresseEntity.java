package api.sms.astro.contact.adaptateur.infrastructure.persistence.entite;

import api.sms.astro.contact.domaine.Pays;
import api.sms.astro.contact.domaine.Ville;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p> Agrégat permettant d'acceder aux objets valeur {@link Pays} et {@link Ville}
 * </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Embeddable
@Table(name = "adresse")
@Getter
@Setter
@NoArgsConstructor
public class AdresseEntity implements Serializable {

  @Column(length = 38)
  private String adressePostal;

  @ManyToOne
  private PaysEntity pays;

  @ManyToOne
  private VilleEntity ville;
}
