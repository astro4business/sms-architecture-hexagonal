package api.sms.astro.contact.adaptateur.infrastructure;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.VilleEntity;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.mapper.VilleMapper;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.repository.VilleJpaRepository;
import api.sms.astro.contact.application.port.out.VillePortRepository;
import api.sms.astro.contact.domaine.Ville;
import java.util.List;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Repository
public class VilleJpaAdapter implements VillePortRepository {

  private final VilleJpaRepository villeJpaRepository;
  VilleMapper mapper = Mappers.getMapper(VilleMapper.class);

  public VilleJpaAdapter(
      VilleJpaRepository villeJpaRepository) {
    this.villeJpaRepository = villeJpaRepository;
  }

  @Override
  public void enregistrer(Ville ville) {

    VilleEntity villeEntity = mapper.villeToEntity(ville);
    this.villeJpaRepository.save(villeEntity);
  }

  @Override
  public void enregistrer(List<Ville> villes) {

    List<VilleEntity> villeEntities = mapper.villesToEntities(villes);
    this.villeJpaRepository.saveAll(villeEntities);
  }

  @Override
  public List<Ville> enregistrerRetourner(List<Ville> villes) {

    List<VilleEntity> villeEntities = mapper.villesToEntities(villes);
    List<VilleEntity> villeEntityList = this.villeJpaRepository.saveAll(villeEntities);
    List<Ville> villeList = mapper.entitiesToVilles(villeEntityList);
    return villeList;
  }

  @Override
  public void supprimer(Ville ville) {
    ville.setSupprimer(true);
    this.enregistrer(ville);
  }

  @Override
  public List<Ville> afficherListes() {

    List<VilleEntity> villeEntities = this.villeJpaRepository
        .findVilleEntitiesBySupprimerFalse();

    return mapper.entitiesToVilles(villeEntities);
  }
}
