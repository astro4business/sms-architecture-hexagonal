package api.sms.astro.contact.adaptateur.infrastructure;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.PaysEntity;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.mapper.PaysMapper;
import api.sms.astro.contact.adaptateur.infrastructure.persistence.repository.PaysJpaRepository;
import api.sms.astro.contact.application.port.out.PaysPortRepository;
import api.sms.astro.contact.domaine.Pays;
import java.util.List;
import java.util.UUID;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Repository
public class PaysJpaAdapter implements PaysPortRepository {

  private final PaysJpaRepository paysJpaRepository;
  PaysMapper mapper = Mappers.getMapper(PaysMapper.class);

  public PaysJpaAdapter(
      PaysJpaRepository paysJpaRepository) {
    this.paysJpaRepository = paysJpaRepository;
  }

  @Override
  public void enregistrer(Pays pays) {

    PaysEntity paysEntity = mapper.paysToEntity(pays);
    this.paysJpaRepository.save(paysEntity);
  }

  @Override
  public Pays enregistrerR(Pays pays) {

    PaysEntity paysEntity = mapper.paysToEntity(pays);
    PaysEntity paysEnregistre = this.paysJpaRepository.save(paysEntity);
    Pays paysARetourner = mapper.entityToPays(paysEnregistre);

    return pays;
  }

  @Override
  public void supprimer(Pays pays) {

    pays.setSupprimer(true);
    this.enregistrer(pays);
  }

  @Override
  public List<Pays> afficherListes() {

    List<PaysEntity> paysEntities = this.paysJpaRepository
        .getPaysEntitiesBySupprimerFalse();
    //return mapper.entitiesToPays(paysEntities);
    return null;
  }

  @Override
  public Pays afficherPays(UUID id) {

    PaysEntity paysEntity = this.paysJpaRepository.findPaysEntityById(id);
    return mapper.entityToPays(paysEntity);
  }

  @Override
  public Pays rechercherPaysParNom(String nom) {

    PaysEntity paysEntity = this.paysJpaRepository.findPaysEntityByNom(nom);
    return mapper.entityToPays(paysEntity);
  }
}
