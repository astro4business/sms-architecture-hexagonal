package api.sms.astro.contact.adaptateur.infrastructure.persistence.repository;//package api.sms.astro.infrastructure.repository;
//
//import api.sms.astro.infrastructure.entite.contact.AdresseEntity;
//import java.util.UUID;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
///**
// * <p>  </p>
// *
// * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
// **/
//public interface AdresseJpaRepository extends JpaRepository<AdresseEntity, UUID> {
//}
