package api.sms.astro.contact.adaptateur.infrastructure.persistence.repository;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.VilleEntity;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Repository
public interface VilleJpaRepository extends JpaRepository<VilleEntity, UUID> {
  List<VilleEntity> findVilleEntitiesBySupprimerFalse();

  @Override
  <S extends VilleEntity> List<S> saveAll(Iterable<S> entities);
}
