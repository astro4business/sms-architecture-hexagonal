package api.sms.astro.contact.adaptateur.infrastructure.persistence.entite;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * <p> Objet valeur permettant l'enregistrement d'une ville </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Entity
@Table(name = "ville")
@Getter
@Setter
@NoArgsConstructor
public class VilleEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column
  private UUID id;

  @Column(nullable = false)
  private String nom;

  @Column(unique = true)
  private String coordonnee;

  @Column(unique = true, nullable = false)
  private String codeISO;

  @Column(columnDefinition = "BOOLEAN DEFAULT false")
  private boolean supprimer;

  @ManyToOne(fetch = FetchType.LAZY)
  private PaysEntity pays;

  @CreationTimestamp
  private LocalDateTime dateCreation = LocalDateTime.now();

  @UpdateTimestamp
  private LocalDateTime dateModification = LocalDateTime.now();

  @PrePersist
  public void actualiseDateCreation() {
    this.dateCreation = LocalDateTime.now();
  }

  @PreUpdate
  public void actualiseDateModification() {
    this.dateModification = LocalDateTime.now();
  }
}
