package api.sms.astro.contact.adaptateur.infrastructure.persistence.mapper;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.VilleEntity;
import api.sms.astro.contact.domaine.Ville;
import java.util.List;
import org.mapstruct.Mapper;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Mapper(componentModel = "spring")
public interface VilleMapper {

  //Ville entityToVille(VilleEntity villeEntity);

  VilleEntity villeToEntity(Ville ville);

  /*@Mapping(target = "villes", ignore = true)
  Pays entityToPays(PaysEntity paysEntity);

  @Mapping(target = "villes", ignore = true)
  PaysEntity paysToEntity(Pays pays);*/

  List<Ville> entitiesToVilles(List<VilleEntity> villeEntities);

  List<VilleEntity> villesToEntities(List<Ville> villes);
}
