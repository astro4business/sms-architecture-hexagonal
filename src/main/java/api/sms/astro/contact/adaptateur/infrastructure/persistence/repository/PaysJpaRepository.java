package api.sms.astro.contact.adaptateur.infrastructure.persistence.repository;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.PaysEntity;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Repository
public interface PaysJpaRepository extends JpaRepository<PaysEntity, UUID> {
  List<PaysEntity> getPaysEntitiesBySupprimerFalse();
  List<PaysEntity> getAllBySupprimerFalse();
  List<PaysEntity> findPaysEntitiesBySupprimerIsFalse();

  @Override
  <S extends PaysEntity> S save(S entity);

  PaysEntity findPaysEntityById(UUID id);

  PaysEntity findPaysEntityByNom(String nom);
}
