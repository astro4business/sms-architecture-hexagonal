package api.sms.astro.contact.adaptateur.infrastructure.persistence.mapper;

import api.sms.astro.contact.adaptateur.infrastructure.persistence.entite.PaysEntity;
import api.sms.astro.contact.domaine.Pays;
import org.mapstruct.Mapper;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
@Mapper(componentModel = "spring")
public interface PaysMapper {

  Pays entityToPays(PaysEntity paysEntity);
  PaysEntity paysToEntity(Pays pays);

  /*@Mapping(target = "pays", ignore = true)
  Ville entityToVille(VilleEntity villeEntity);

  @Mapping(target = "pays.villes", ignore = true)
  VilleEntity villeToEntity(Ville ville);

  List<Pays> entitiesToPays(List<PaysEntity> paysEntities);
  List<PaysEntity> paysToEntities(Set<Pays> pays);*/
}
