package api.sms.astro.contact.adaptateur.userinterface.rest;

import api.sms.astro.GestionnaireCommande;
import api.sms.astro.contact.application.commande.CreerContactGC;
import api.sms.astro.contact.application.port.in.ContactCommande;
import api.sms.astro.contact.application.port.out.ContactPortRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/** @author Touré Ahmed Christian Cédrick | Date : 16/08/2020 */
@RestController
public class ContactRestController {

  private final GestionnaireCommande<ContactCommande> gestionnaireCommandeContact;

  // Injection de dépendance
  public ContactRestController(ContactPortRepository contactPortRepository) {
    this.gestionnaireCommandeContact = new CreerContactGC(contactPortRepository);
  }

  @PostMapping("api/creer/contact")
  @ResponseStatus(HttpStatus.CREATED)
  public void creerContact(@RequestBody ContactCommande contactCommande) {
    this.gestionnaireCommandeContact.execute(contactCommande);
  }
}
