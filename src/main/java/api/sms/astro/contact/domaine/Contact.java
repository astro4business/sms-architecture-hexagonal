package api.sms.astro.contact.domaine;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * <p> Entité domaine Contact </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Builder
@AllArgsConstructor
public class Contact implements Serializable {

  private UUID id;
  private String email;
  private String numeroTelephone;
  private String numeroFixe;

  private Adresse adresse;
  //private Utilisateur utilisateur;

  private boolean supprimer = false;

  public Contact() {
    super();
  }

  public Contact(String email, String numeroTelephone, String numeroFixe) {
    this.email = email;
    this.numeroTelephone = numeroTelephone;
    this.numeroFixe = numeroFixe;
  }

  public Contact(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNumeroTelephone() {
    return numeroTelephone;
  }

  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }

  public String getNumeroFixe() {
    return numeroFixe;
  }

  public void setNumeroFixe(String numeroFixe) {
    this.numeroFixe = numeroFixe;
  }

  public Adresse getAdresse() {
    return adresse;
  }

  public void setAdresse(Adresse adresse) {
    this.adresse = adresse;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Contact contact = (Contact) o;
    return Objects.equals(id, contact.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, email, numeroTelephone, numeroFixe, adresse);
  }

  @Override
  public String toString() {
    return "Contact{" +
        "id=" + id +
        ", email='" + email + '\'' +
        ", numeroTelephone='" + numeroTelephone + '\'' +
        ", numeroFixe='" + numeroFixe + '\'' +
        ", adresse=" + adresse +
        '}';
  }
}
