package api.sms.astro.contact.domaine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * <p> Objet valeur permettant l'enregistrement d'un pays </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Builder
@AllArgsConstructor
public class Pays implements Serializable {

  private UUID id;
  private String nom;
  private String coordonnee;
  private String codeISO;
  private boolean supprimer = false;
  private List<Ville> villes = new ArrayList<>();

  public Pays() {
    super();
  }

  public Pays(String nom) {
    this.nom = nom;
  }

  public Pays(String nom, String coordonnee, String codeISO,
      List<Ville> villes) {
    this.nom = nom;
    this.coordonnee = coordonnee;
    this.codeISO = codeISO;
    this.villes = villes;
  }

  public Pays(String nom, String coordonnee,
      List<Ville> villes) {
    this.nom = nom;
    this.coordonnee = coordonnee;
    this.villes.addAll(villes);
  }

  public void ajouterVille(Ville ville) {
    villes.add(ville);
  }

  public void ajouterVilles(List<Ville> villes) {
    this.villes.addAll(villes);
  }

  public void supprimerVille(Ville ville) {
    villes.remove(ville);
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCoordonnee() {
    return coordonnee;
  }

  public void setCoordonnee(String coordonnee) {
    this.coordonnee = coordonnee;
  }

  public String getCodeISO() {
    return codeISO;
  }

  public void setCodeISO(String codeISO) {
    this.codeISO = codeISO;
  }

  public List<Ville> getVilles() {
    return villes;
  }

  public void setVilles(List<Ville> villes) {
    this.villes = villes;
//    villes.forEach(this::ajouterVille);
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  public void reinitialiserVilles() {
    this.villes.clear();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Pays pays = (Pays) o;
    return Objects.equals(id, pays.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, coordonnee, codeISO, villes);
  }

  @Override
  public String toString() {
    return "Pays{" +
        "id=" + id +
        ", nom='" + nom + '\'' +
        ", coordonnee='" + coordonnee + '\'' +
        ", codeISO='" + codeISO + '\'' +
        ", villes=" + villes +
        '}';
  }
}
