package api.sms.astro.contact.domaine;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * <p> Objet valeur permettant l'enregistrement d'une ville </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Builder
@AllArgsConstructor
public class Ville implements Serializable {

  private UUID id;
  private String nom;
  private String coordonnee;
  private String codeISO;
  private boolean supprimer = false;
  private Pays pays;

  public Ville() {
    super();
  }

  public Ville(String nom) {
    this.nom = nom;
  }

  public Ville(String nom, String coordonnee) {
    this.nom = nom;
    this.coordonnee = coordonnee;
  }

  public Ville(String nom, String coordonnee, String codeISO) {
    this.nom = nom;
    this.coordonnee = coordonnee;
    this.codeISO = codeISO;
  }

  public Pays getPays() {
    return pays;
  }

  public void setPays(Pays pays) {
    this.pays = pays;
  }

  public boolean isSupprimer() {
    return supprimer;
  }

  public void setSupprimer(boolean supprimer) {
    this.supprimer = supprimer;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCoordonnee() {
    return coordonnee;
  }

  public void setCoordonnee(String coordonnee) {
    this.coordonnee = coordonnee;
  }

  public String getCodeISO() {
    return codeISO;
  }

  public void setCodeISO(String codeISO) {
    this.codeISO = codeISO;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ville ville = (Ville) o;
    return Objects.equals(id, ville.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, coordonnee, codeISO);
  }

  @Override
  public String toString() {
    return "Ville{" +
        "id=" + id +
        ", nom='" + nom + '\'' +
        ", coordonnee='" + coordonnee + '\'' +
        ", codeISO='" + codeISO + '\'' +
        ", supprimer=" + supprimer +
        '}';
  }
}
