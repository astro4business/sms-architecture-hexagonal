package api.sms.astro.contact.domaine;

import java.io.Serializable;
import java.util.Objects;
import lombok.Builder;

/**
 * <p> Agrégat permettant d'acceder aux objets valeur {@link Pays} et {@link Ville}
 * </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 11/08/2020
 **/
@Builder
public class Adresse implements Serializable {

  private String adressePostal;
  
  private Pays pays;
  private Ville ville;

  public Adresse() {
    super();
  }

  public Adresse(String adressePostal,
      Pays pays, Ville ville) {
    this.adressePostal = adressePostal;
    this.pays = pays;
    this.ville = ville;
  }

  public Adresse(String adressePostal) {
    this.adressePostal = adressePostal;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Adresse adresse = (Adresse) o;
    return Objects.equals(adressePostal, adresse.adressePostal) &&
        Objects.equals(pays, adresse.pays) &&
        Objects.equals(ville, adresse.ville);
  }

  @Override
  public int hashCode() {
    return Objects.hash(adressePostal, pays, ville);
  }

  @Override
  public String toString() {
    return "Adresse{" +
        "adressePostal='" + adressePostal + '\'' +
        ", pays=" + pays.getNom() +
        ", ville=" + ville.getNom() +
        '}';
  }

  public String getAdressePostal() {
    return adressePostal;
  }

  public void setAdressePostal(String adressePostal) {
    this.adressePostal = adressePostal;
  }

  public Pays getPays() {
    return pays;
  }

  public void setPays(Pays pays) {
    this.pays = pays;
  }

  public Ville getVille() {
    return ville;
  }

  public void setVille(Ville ville) {
    this.ville = ville;
  }
}
