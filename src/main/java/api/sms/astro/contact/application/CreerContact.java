package api.sms.astro.contact.application;

import api.sms.astro.contact.application.port.out.ContactPortRepository;
import api.sms.astro.contact.application.port.in.ContactCommande;
import api.sms.astro.contact.domaine.Adresse;
import api.sms.astro.contact.domaine.Contact;
import api.sms.astro.contact.domaine.Pays;
import api.sms.astro.contact.domaine.Ville;

/** @author Touré Ahmed Christian Cédrick | Date : 16/08/2020 */
public class CreerContact {

  private final ContactPortRepository contactPortRepository;

  public CreerContact(ContactPortRepository contactPortRepository) {
    this.contactPortRepository = contactPortRepository;
  }

  public void creer(ContactCommande commande) {

    Contact contact = this.genererContact(commande);
    this.contactPortRepository.enregistrer(contact);
  }

  public Contact genererContact(ContactCommande commande) {

    commande.validation();

    Contact contact =
        Contact.builder()
            .email(commande.getEmail())
            .numeroTelephone(commande.getNumeroTelephone())
            .numeroFixe(commande.getNumeroFixe())
            .adresse(
                Adresse.builder()
                    .adressePostal(commande.getAdressePostal())
                    .pays(
                        Pays.builder()
                            .nom(commande.getNomPays())
                            .coordonnee(commande.getCoordonneePays())
                            .codeISO(commande.getCodeISOPays())
                            .build())
                    .ville(
                        Ville.builder()
                            .nom(commande.getNomVille())
                            .coordonnee(commande.getCoordonneeVille())
                            .codeISO(commande.getCodeISOVille())
                            .build())
                    .build())
            .build();

    return contact;
  }
}
