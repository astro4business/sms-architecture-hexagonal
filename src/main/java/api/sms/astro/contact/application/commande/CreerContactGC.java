package api.sms.astro.contact.application.commande;

import api.sms.astro.GestionnaireCommande;
import api.sms.astro.contact.application.CreerContact;
import api.sms.astro.contact.application.port.out.ContactPortRepository;
import api.sms.astro.contact.application.port.in.ContactCommande;

/** @author Touré Ahmed Christian Cédrick | Date : 16/08/2020 */
public class CreerContactGC implements GestionnaireCommande<ContactCommande> {

  private final CreerContact creerContact;

  public CreerContactGC(ContactPortRepository contactPortRepository) {
    this.creerContact = new CreerContact(contactPortRepository);
  }

  @Override
  public void execute(ContactCommande commande) {
    this.creerContact.creer(commande);
  }
}
