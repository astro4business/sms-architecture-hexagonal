package api.sms.astro.contact.application.port.in;

/**
 * <b>  </b>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 03/10/2021
 **/
public interface FusionnerContactUseCase {

  void fusionner(ContactCommande commande);
}
