package api.sms.astro.contact.application.port.out;

import api.sms.astro.contact.domaine.Pays;
import java.util.List;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 18/08/2020
 **/
public interface PaysPortRepository {

  void enregistrer(Pays pays);

  Pays enregistrerR(Pays pays);

  void supprimer(Pays pays);

  List<Pays> afficherListes();

  Pays afficherPays(UUID id);

  Pays rechercherPaysParNom(String nom);
}
