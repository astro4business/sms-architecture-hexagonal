package api.sms.astro.contact.application.port.in;

import api.sms.astro.contact.ValidationCommande;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FusionContactCommande extends ValidationCommande<FusionContactCommande> {

  @NotBlank
  private List<UUID> idContacts;
}
