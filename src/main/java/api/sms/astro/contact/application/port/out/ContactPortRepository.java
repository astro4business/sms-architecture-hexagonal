package api.sms.astro.contact.application.port.out;

import api.sms.astro.contact.domaine.Contact;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 15/08/2020
 **/
public interface ContactPortRepository {

  void enregistrer(Contact contact);

  void supprimer(Contact contact);

  List<Contact> afficherListesContacts();
}
