package api.sms.astro.contact.application.port.out;

import api.sms.astro.contact.domaine.Ville;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 17/08/2020
 **/
public interface VillePortRepository {

  void enregistrer(Ville ville);

  void enregistrer(List<Ville> villes);

  List<Ville> enregistrerRetourner(List<Ville> villes);

  void supprimer(Ville ville);

  List<Ville> afficherListes();
}
