package api.sms.astro.contact.application.port.in;

import api.sms.astro.contact.ValidationCommande;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>  </p>
 *
 * @author Touré Ahmed Christian Cédrick | Date : 16/08/2020
 **/
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactCommande extends ValidationCommande<ContactCommande> {

  @Email(message = "L'adresse email est incorrect.")
  private String email;

  @NotBlank(message = "Le numéro de téléphone est incorrect.")
  @Size(min = 3)
  private String numeroTelephone;

  @NotNull(message = "Le numéro fixe est incorrect.")
  @Size(min = 3)
  private String numeroFixe;

  @Size(min = 3, message = "L'adresse postal est incorrect.")
  private String adressePostal;

  // Pays

  @Size(min = 2, message = "Le nom du pays doit être supérieur à 2 caractères.")
  private String nomPays;

  @Size(min = 2, message = "Les coordonnées du pays doivent être supérieur à 2 caractères.")
  private String coordonneePays;

  @Size(min = 2, message = "Le code iso du pays doivent être supérieur à 2 caractères.")
  private String codeISOPays;

  // Ville

  @Size(min = 2, message = "Le nom de la ville doit être supérieur à 2 caractères.")
  private String nomVille;

  @Size(min = 2, message = "Les coordonnées de la ville doivent être supérieur à 2 caractères.")
  private String coordonneeVille;

  @Size(min = 2, message = "Le code iso du pays doivent être supérieur à 2 caractères.")
  private String codeISOVille;
}
